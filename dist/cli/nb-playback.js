#!/usr/bin/env node --harmony
'use strict';

var yargs = require('yargs') // eslint-disable-line
.command(['lmdb2osc', '*'], 'Play back an LMDB file as OSC packets (real-time)', function () {}, function (argv) {
  process.env.IN_FILE = argv.infile;
  process.env.FPS = argv.fps;
  process.env.ADDR_LOCAL = argv.local;
  process.env.ADDR_REMOTE = argv.remote;
  process.env.OSC_ADDRESS = argv.address;
  process.env.DEBUG_MODE = argv.debug;
  require('../nanobrains/reduce');
}).option('infile', {
  alias: 'i',
  describe: 'LMDB input file',
  required: true
}).option('fps', {
  alias: 'f',
  describe: 'Target frames per second',
  default: '50.0'
}).option('local', {
  alias: 'l',
  describe: 'Local OSC address (listen)',
  default: '127.0.0.1:8888'
}).option('remote', {
  alias: 'r',
  describe: 'Remote OSC address (send)',
  default: '127.0.0.1:9999'
}).option('address', {
  alias: 'a',
  describe: 'Override default OSC address'
}).option('debug', {
  alias: 'd',
  default: false
}).help().argv;